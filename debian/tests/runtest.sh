#!/bin/sh

set -e

#trap "journalctl -u redeye | tail -n 100" EXIT

redeye

if [ ! -d ~/.redeye ]; then
    echo "The directory ~/.redeye has not been created"
    exit 1
fi

if ! systemctl is-active -q redeye; then
    echo "The service redeye fails to start"
    exit 1
fi

if ! curl 127.0.0.1:8443 | grep "Redeye Login"; then
    echo "FAILURE: Web UI test failed"
    exit 1
fi

if ! curl -d 'username=redeye' -d 'password=redeye' -d 'project=example.db' 127.0.0.1:8443/login | grep "Enter your thoughts"; then
    echo: "FAILURE: Login failed"
    exit 1
fi

redeye-stop
if systemctl is-active -q redeye; then
    echo "The service redeye is still active"
    exit 1
fi
